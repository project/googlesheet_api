<?php

/**
 * @file
 * Google Sheet API Form.
 */

/**
 * Implements hook_form().
 */
function google_sheet_api_add_form() {
  $form = array();
  $google_sheet_path = '<a href="https://docs.google.com/spreadsheets/d/' . variable_get('sheet_id') . '/edit#gid=' . variable_get('group_id') . '">View Google Spread Sheet</a>';

  $add_top_link = <<<STR
    <p> $google_sheet_path </p>
STR;

  $form['data_table_add_link'] = array(
    '#name' => 'data_table',
    '#markup' => $google_sheet_path,
  );

  $form['custom_google_sheet_api'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom Contact Form'),
    '#collapsible' => FALSE,
  );

  $form['custom_google_sheet_api']['contact_firstname'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),
    '#prefix' => '<div class="show-hide-galileo_id">',
    '#suffix' => '</div>',
    '#attributes' => array('style' => 'width:350px;', 'placeholder' => ''),
    '#required' => TRUE,
  );

  $form['custom_google_sheet_api']['contact_lastname'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#prefix' => '<div class="show-hide-galileo_id">',
    '#suffix' => '</div>',
    '#attributes' => array('style' => 'width:350px;', 'placeholder' => ''),
    '#required' => TRUE,
  );

  $form['custom_google_sheet_api']['contact_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email Address'),
    '#prefix' => '<div class="show-hide-galileo_id">',
    '#suffix' => '</div>',
    '#attributes' => array('style' => 'width:350px;', 'placeholder' => ''),
    '#required' => TRUE,
  );

  $form['custom_google_sheet_api']['contact_phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
    '#prefix' => '<div class="show-hide-galileo_id">',
    '#suffix' => '</div>',
    '#attributes' => array('style' => 'width:350px;', 'placeholder' => ''),
    '#required' => TRUE,
  );

  $form['custom_google_sheet_api']['contact_country'] = array(
    '#type' => 'textfield',
    '#title' => t('Country'),
    '#prefix' => '<div class="show-hide-galileo_id">',
    '#suffix' => '</div>',
    '#attributes' => array('style' => 'width:350px;', 'placeholder' => ''),
  );

  $form['custom_google_sheet_api']['contact_city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#prefix' => '<div class="show-hide-galileo_id">',
    '#suffix' => '</div>',
    '#attributes' => array('style' => 'width:350px;', 'placeholder' => ''),
  );

  $form['custom_google_sheet_api']['contact_description'] = array(
    '#type' => 'textarea',
    '#rows' => 10,
    '#columns' => 40,
    '#title' => t('Description'),
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#default_value' => t('Submit'),
    '#prefix' => '<div id="dblog-admin-buttons" style="float:left;margin: 10px 10px 0 0;">',
    '#suffix' => '</div>',
  );

  return $form;
}

/**
 * Implements hook_submit().
 */
function google_sheet_api_add_form_submit($form, &$form_state) {
  $date = date('Y-m-d');
  $contact_firstname = trim(check_plain($form_state['values']['contact_firstname']));
  $contact_lastname = trim(check_plain($form_state['values']['contact_lastname']));
  $contact_email = trim(check_plain($form_state['values']['contact_email']));
  $contact_phone = trim(check_plain($form_state['values']['contact_phone']));
  $contact_country = trim(check_plain($form_state['values']['contact_country']));
  $contact_city = trim(check_plain($form_state['values']['contact_city']));
  $contact_description = trim(check_plain($form_state['values']['contact_description']));

  require __DIR__ . '/includes/lib/vendor/autoload.php';
  $client = new Google_Client();

  $client->addScope(Google_Service_Sheets::SPREADSHEETS);
  $client->setAccessType('offline');
  $json_auth = variable_get('json_auth');
  $client->setAuthConfig(json_decode($json_auth, TRUE));
  $data = array(
    'date' => $date,
    'contact_firstname' => $contact_firstname,
    'contact_lastname' => $contact_lastname,
    'contact_email' => $contact_email,
    'contact_phone' => $contact_phone,
    'contact_country' => $contact_country,
    'contact_city' => $contact_city,
    'contact_description' => $contact_description,
  );

  $sheet_id = variable_get('sheet_id');
  $sheet_service = new Google_Service_Sheets($client);
  $file_id = $sheet_id;
  $values = array();
  foreach ($data as $d) {
    $cell_data = new Google_Service_Sheets_CellData();
    $value = new Google_Service_Sheets_ExtendedValue();
    $value->setStringValue($d);
    $cell_data->setUserEnteredValue($value);
    $values[] = $cell_data;
  }

  // Build the RowData.
  $row_data = new Google_Service_Sheets_RowData();
  $row_data->setValues($values);
  // Prepare the request.
  $append_request = new Google_Service_Sheets_AppendCellsRequest();
  // Copy & paste from a spreadsheet URL gid.
  $group_id = variable_get('group_id');
  $append_request->setSheetId($group_id);
  $append_request->setRows($row_data);
  $append_request->setFields('userEnteredValue');
  // Set the request.
  $request = new Google_Service_Sheets_Request();
  $request->setAppendCells($append_request);

  // Add the request to the requests array.
  $requests = array();
  $requests[] = $request;

  // Prepare the update.
  $batch_update_request = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(array(
    'requests' => $requests,
  ));

  try {
    // Execute the request.
    $response = $sheet_service->spreadsheets->batchUpdate($file_id, $batch_update_request);
    if ($response->valid()) {
      drupal_set_message(t('Record has been created sucessfully.'));
    }
  }
  catch (Exception $e) {
    drupal_set_message(t('Invalid Google Sheet API configuration.'), 'error');
  }
}

/**
 * Implements hook_validate().
 */
function google_sheet_api_add_form_validate($form, &$form_state) {
  if (!isset($form_state['values']['contact_firstname']) || empty($form_state['values']['contact_firstname'])) {
    form_set_error('contact_firstname', t('Please enter firstname'));
  }

  if (!isset($form_state['values']['contact_lastname']) || empty($form_state['values']['contact_lastname'])) {
    form_set_error('contact_lastname', t('Please enter lastname'));
  }

  if (!isset($form_state['values']['contact_email']) || empty($form_state['values']['contact_email'])) {
    form_set_error('contact_email', t('Please enter Email'));
  }

  if (!isset($form_state['values']['contact_phone']) || empty($form_state['values']['contact_phone'])) {
    form_set_error('contact_phone', t('Please enter phone'));
  }

  if (strlen(trim($form_state['values']['contact_firstname'])) < 2) {
    form_set_error('contact_firstname', "First Name must have at least 2 characters.");
  }

  if (strlen(trim($form_state['values']['contact_lastname'])) < 2) {
    form_set_error('contact_lastname', "Last Name must have at least 2 characters.");
  }

  if (!empty($form_state['values']['contact_phone'])) {
    if (strlen($form_state['values']['contact_phone']) > 19) {
      form_set_error('contact_phone', t('Phone Number cannot be more than 19 characters.'));
    }
  }

  if (FALSE == filter_var($form_state['values']['contact_email'], FILTER_VALIDATE_EMAIL)) {
    form_set_error('contact_email', "Invalid email address");
  }

}
